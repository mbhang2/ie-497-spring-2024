# Strategy Development Independent Study

[[_TOC_]]

## 1. Abstract

This strategy generation project features both maker and taker strategy improved upon the previous [project](https://gitlab.engr.illinois.edu/fin556_algo_market_micro_fall_2023/fin556_algo_fall_2023_group_02/group_02_project), utilizing trade, quote, and order book update data for the US cash equities market from the IEX exchange, as well as trade data for Futures and cryptocurrencies from CME and Binance, respectively. 

The research methodology involves the development and implementation of a comprehensive algorithmic trading framework that combines elements of both retail and HFT strategies. The selected market data provides a granular view of market dynamics, enabling a detailed examination of trading signals, order execution, and market impact.

The results revealed positive performace for both maker and taker strategies backtested in various markets. Maker strategy has not only had positive net pnl solely from the trades, but also from fees due to IEX's **rebate** fee schedule upon posting liquidity on the orderbook. Taker strategy has performed exceptionally in riding the trend in various markets. 

## 2. Introduction

### 2.1 Researcher Profile

<img src="./team_photos/michael.png" alt="" width="400"/>

**Min Hyuk (Michael) Bhang** <br><br>
I am Min Hyuk (Michael) Bhang, an undergraduate majoring in Industiral Engineering (Track option: Economics and Finance) and minoring in Computer Science at the University of Illinois at Urbana-Champaign. My professional industry experience includes investigating Fintech utilizing blockchain technology to prevent fraud and anonymize personal data, and connecting crypto consultee with venture capitals and private equities to raise funds for seed and early stages in East Asia. Alongside the industry experience, I have strong interest and background in day-trading cash equities, futures, options, and cryptocurrencies with 5 years of experience. I currently scalp/daytrade NASDAQ futures (NQ) only on a daily basis as a contracted daytrader at a proprietary firm with AUM of $500k.
    
- Email: [mbhang2@illinois.edu](/fin556_algo_market_micro_fall_2023/fin556_algo_fall_2023_group_02/group_02_project/-/blob/main/mbhang2@illinois.edu) 
- LinkedIn: [Min Hyuk Bhang](https://www.linkedin.com/in/michael-bhang/)

### 2.2 The Idea 

In order to better understand the similarities and differences between high-frequency trading (HFT) and the retail trading, I implemented some common retail strategies in a high frequency context. Specifically, retail concepts that have been explored throughout this study are Range Bar Charts, Renko charts, Bollinger Bands, Pivot Points, Demark Pivot Points, and Support/Resistance. My goal throughout the research project is to gain a better understanding of market microstructure, trading strategy development and backtesting, and lay the foundation for a deeper dive into HFT. 

### 2.3 Market Microstructure

Market microstructure refers to the intricate organizational and operational elements of financial markets that influence the trading of assets. It encompasses the processes related to order placement, price determination, trade execution, and the flow of information within a market.

#### 2.3.1 Bid and Ask

**Bid Price** 
* The maximum price as which a buyer is willing to pay for a security.
* The best bid price is the highest bid price.
* Represents the demand side of the market.

**Ask (Offer) Price**
* The minimum price at which a seller is willing to sell a security.
* The best ask price is the lowest ask price.
* Represents the supply side of the market.

**Bid-Ask Spread**
* The difference between the best bid and offer (BBO).
* Reflects the cost of trading and represents the profit potential for market makers.
* Narrower spread generally indicates higher liquidity.

#### 2.3.2 Order Book

![Order book image](./image/orderbook.png)

The order book is a dynamic, real-time electronic record of buy and sell orders organized by price level for a specific security. It plays a central role in market microstructure, providing transparency into the current supply and demand by informing on price, availability, depth of trade, and who initiates transactions. 

Three major components of an order book are: 

* Buy Orders (Bids):
    * Listed in descending order, with the best bid at the top.
    * Accumulation of buy orders at a specific price point can indicate a support level.

* Sell Orders (Asks or Offers):
    * Listed in ascending order, with the best ask presented first.
    * Likewise, accumulation of sell orders at a specific price point can indicate a resistance level.

* Order History:
    * Refers to the historical data of executed trades that have occurred.
    * Provides insights into past transactions and helps traders analyze market behavior.

#### 2.3.3 Latency

Latency refers to the time delay between the initiation (sending out) of a trade and its receival. In our backtester, order submission-reception latency is considered.

#### 2.3.4 Updates

**Trade Update**: Called when trade occurred.

**Quote Update**: Called when top quote (best bid or ask) price and/or size is updated.

**Depth Update**: Called when non-best bid or ask price and/or size is updated.

#### 2.3.5 Order Type

Four major type of orders are: Limit, Market, Stop, and Stop Limit.

**Limit**: 
* Allows investors to buy or sell securities at a specific price in the future.
* Trader sets the maximum or minimum price at which the trader is willing to buy or sell.
* The order will only be filled if the market reaches the pre-defined level.
* Limit orders provide **control over execution price** but **no guarantee of execution**.

![limit order](./image/limit.png)

**Market**:
* Instructs the exchange to buy or sell immediately at the current market price.
* When placed:
    * If buying, trader will pay a price at or near the posted ask (selling) price.
    * If selling, trader will receive a price at or near the posted bid (buying) price.
* Market orders **guarantee immediate execution** but **not a specific price**.

![market order](./image/market.png)

**Stop**:
* Becomes a market order when the stock reaches a specified price.
* Often used to limit losses or lock in profits.

![stop order](../image/stop.png)

**Stop Limit**:
* Combines features of stop and limit orders; becomes a limit order when the stock reaches a specified price.
* Trader sets both a stop price (when the order becomes active) and a limit price (the maximum or minimum price you’re willing to accept).

![stop limit order](../image/stoplimit.png)

#### 2.3.6 Market "Making" vs Market "Taking"

In financial markets, the interplay between liquidity providers and liquidity consumers is fundamental to the efficient functioning of trading systems. Two key roles emerge from this dynamic: market makers and market takers. Each plays a distinct part in shaping market behavior and ensuring the smooth execution of trades.

**Market Making**

Market makers play a crucial role in financial markets. They are like the architects who construct the framework for trading. A market maker’s primary goal is to provide liquidity by continuously offering to buy and sell a particular security by maintaining a tight bid-ask spread. Market makers profit from the difference between these two prices (the spread). They constantly adjust their quotes based on market conditions, ensuring that buyers and sellers can execute trades efficiently. In return for providing liquidity, market makers receive a premium from market takers.

**Market Taking**

Conversely, market takers are the swift actors. Market takers prioritize immediacy over getting the best price, and take the existing bid or ask price set by market makers. By executing trades promptly, market takers ensure that liquidity remains available. 

### 2.4 Retail Trading Tools

#### 2.4.1 Candlestick

![candlestick](./image/candlestick.png)

A candlestick is a powerful visual representation of price movement for a specific financial security over a given period. Originating from Japanese rice merchants and traders centuries ago, candlesticks have become a fundamental tool in modern financial markets.

Candlestick charts display the high, low, open, and closing prices of a security during a specific timeframe. Each individual candlestick represents a single period (e.g., a day, an hour, or even minutes). The wide part of the candlestick is called the “real body.” It indicates whether the closing price was higher or lower than the opening price. If the stock closed lower, the real body is usually black/red; if it closed higher, the real body is typically white/green.

#### 2.4.2 Renko Chart

![renko](./image/renko.png)

A Renko chart is a type of trading chart that provides market data in a different way compared to more traditional charts like candlestick, HLOC (High-Low-Open-Close), and Heikin Ashi. Instead of using fixed time intervals, Renko charts are comprised of boxes or bricks that represent significant pricing trends. Here are the key points about Renko charts:

Renko charts are built using price movement rather than both price and standardized time intervals. Each brick represents a specific price movement, rather than a fixed period of time. The chart looks like a series of bricks, hence the name “Renko,” which is thought to be derived from the Japanese word for bricks, “renga.”

**Brick Construction**:

Renko charts are composed of bricks that represent price movements.
Each brick is created at a 45-degree angle (up or down) from the prior brick.
Unlike other charts, consecutive bricks do not occur directly beside each other.

**Box Size**:
	
The user determines the brick size for the chart. This size represents the magnitude of price movement. For example, a stock might have a $0.25 box size, while a currency pair could have a 50-pip box size. ATR (Average True Range) can also be used to set the box size.

**Brick Formation**:
	
A new brick is drawn in the next column once the price surpasses the top or bottom of the previous brick by the specified box size. For instance, if a stock is trading at $10 with a $0.25 box size, a new brick will form when the price closes at $10.25 or higher. If the price only reaches $10.24, no new brick is drawn.

**Filtering Noise**:

Renko charts filter out minor price movements, making it easier to spot important trends. However, some price information is lost due to the simple brick construction.

**Time Axis**:

Renko charts have a time axis, but the time scale is not fixed. Some bricks may take longer to form than others, depending on how long it takes the price to move the required box size.

***In the taker-strategy, Renko Chart was used to better supress the noise and reveal the trend.***

#### 2.4.3 Bollinger Bands

![bollinger bands](./image/bb.png)

A Bollinger Band is a widely used technical analysis tool that helps investors and traders gauge the volatility of stocks and other securities. Developed by financial analyst John Bollinger in the 1980s, these bands appear on stock charts as three lines that move with the price. Here’s how they work:

**Composition**:

Bollinger Bands consist of three lines:

* The center line represents the stock price’s 20-day simple moving average (SMA).
* The upper band is set at a certain number of standard deviations (usually two) above the middle line.
* The lower band is similarly set below the middle line.

As the stock price becomes more volatile, the bands widen, and they contract when the price stabilizes.

**Interpretation**:

* When a stock’s price nears the upper band, it is often considered overbought.
* Conversely, as the price approaches the lower band, it is seen as oversold.
* These conditions can signal opportune times for trading decisions.

**Secondary Indicator**:

Bollinger Bands are valuable but are best used to confirm other analysis methods. Traders often combine them with other indicators for a comprehensive view of market conditions.

***In the taker-strategy, Bollinger Bands were used with a shift to indicate the start of the trend when it breaks above/below the shifted band of lines.***

#### 2.4.4 Support and Resistance

![sr](./image/sr.png)

Support and resistance are fundamental concepts used to understand price movements and potential reversals. These levels serve as critical reference points on price charts, aiding traders in making informed decisions.

**Support** represents a price level or zone where a stock price tends to find buying pressure. When a stock price drops to a level that prompts traders to buy, it creates support. This level indicates that demand overwhelms supply, causing the price decline to halt and potentially reverse. On a price chart, support appears as a horizontal line or a zone where buyers step in to prevent further price declines. Traders often view support levels as potential entry points for long positions.

**Resistance**, on the other hand, is the opposite of support. It represents a price level or zone where selling pressure tends to emerge. As prices move higher, there comes a point where selling overwhelms the desire to buy. Traders may consider prices too high or have met their profit targets, leading to resistance. Resistance levels appear as horizontal lines or zones where price advances stall temporarily. Traders often view resistance levels as potential exit points for long positions or as areas to initiate short positions.

***In the market-making strategy, support and resistance were utilized to find potential price levels where buyers and sellers will "battle" and take advantage of that volume.***

#### 2.4.5 Pivot Points

![pivot points](./image/pivotpoints.png)

Pivot point is an intraday technical indicator used to identify trends and reversals mainly in equities, commodities, and forex markets. Pivot points are calculated to determine levels in which the sentiment of the market could change from bullish to bearish, and vice-versa. Following are the computations for each levels:

* **Pivot Point (P)**:

	* P: (H + L + C) / 3

* **Resistance Levels (R1 and R2)**:

	* R1: P * 2 - Low
	* R2: P + (H - L)

* **Support Levels (S1 and S2)**:

	* S1: P * 2 - High
	* S2: P - (H - L)

#### 2.4.6 Demark Levels

![demark](./image/demark.png)

DeMark pivot points, also known as TD Points, are an advanced form of pivot points used in technical analysis to predict potential price reversals in financial markets. These points are calculated using a unique combination of highs, lows, and closing prices of the previous trading session or period. Unlike traditional pivot points, DeMark pivot points offer a more focused approach by considering fewer support and resistance levels.

* **DeMark Pivot Point (D)**:

	* If C < O: (H + 2 * L + C) / 4
	* If C > O: (2 * H + L + C) / 4
	* IF C = O: (H + L + 2 * C) / 4

* **DeMark Support (S)**:

	* S: D * 2 - H

* **DeMark Resistance (R)**:

	* R: D * 2 - L

## 3. Tools and Technologies

### 3.1 Programming Languages
- **C++:** A high-performance programming language widely used in finance for its efficiency and speed. It is particularly suitable for developing components of the trading strategy that require low-level memory manipulation and optimal execution. It was employed for performance-critical components of the strategy.

- **Python:** A versatile and widely adopted programming language known for its readability and ease of use. Python is often chosen for rapid development, data manipulation, and as a general-purpose language. It was the primary language for overall strategy development and implementation.

### 3.2 Frameworks
- **[Pandas](https://pandas.pydata.org/):** A powerful data manipulation library for Python, providing data structures like DataFrames that are essential for handling and analyzing structured data efficiently. It was used for data manipulation. 

- **[NumPy](https://numpy.org/):** A fundamental package for scientific computing with Python, enabling efficient handling of large, multi-dimensional arrays and matrices, along with mathematical functions to operate on these data structures. It was used for numerical operations. 

- **[Matplotlib](https://matplotlib.org):** A comprehensive 2D plotting library for Python. It produces publication-quality figures in a variety of formats and interactive environments across platforms. Matplotlib can be used to generate plots, histograms, power spectra, bar charts, error charts, scatterplots, and more. It serves as the foundation for many other plotting libraries and is highly customizable.

### 3.3 Software and Libraries

- **[RCM Strategy Studio](https://rcm-x.com/principal-trading-software/):** RCM-X generously offered us a licence for its principal trading Software, Strategy Studio. Strategy Studio is a trading software designed for backtesting and optimizing trading strategies, providing a comprehensive environment for evaluating the performance of algorithms.

- **[GitLab](https://gitlab.com/):** A web-based Git repository manager that provides version control, collaboration, and continuous integration features. GitLab was used to manage and track changes to the source code, facilitating collaborative development and maintaining a version history of the project.

## 4. Data  
Data quality is a cornerstone of High Frequency Trading (HFT) and quantitative trading strategies. These strategies rely on mathematical models to make rapid trading decisions and are limited by the quality of the data used to develop and trade them. Quality data ensures robust, adaptable models that reflect the dynamic nature of markets. Further, HFT strategies often involve exploiting small price differentials or inefficiencies in the market. To effectively capitalize on these imbalances, analyzing high quality data will provide insight into order book dynamics, trade flows, and price movements at a granular level. Once a strategy has been developed, historical data is used to backtest and evaluate the performance of a strategy. Accurate historical data is essential for assessing the viability of a strategy and understanding its potential risks and returns. 

In order to ensure that I am building our strategies on a strong foundation, I evaluated and used several sources. I implemented strategies on various financial assets: US cash equities, Futures, and crypto currency.

### 4.1 Data Sources
#### 4.1.1 US Cash Equities 

**IEX TOPS**

Investors Exchange (IEX)'s TOPS data feed provides real-time top-of-book quotations, last sale information, short sale restriction status, regulatory trading status, and auction details directly from IEX. The aggregated size of quotations received via TOPS does not reveal individual order details at the best bid or ask, but aggregated orders at the price level. Last trade price and size information is provided, reporting trades from both displayed and non-displayed orders on IEX, while routed executions are not reported. TOPS focuses specifically on the top of the order book. We utilized the tick data in our preliminary analysis. 

**IEX DEEP**

The Investors Exchange (IEX)'s DEEP data feed offers real-time depth of book quotations and last sale information for IEX-listed securities. It provides the aggregated size of resting displayed orders at a given price and side during auctions, along with security-related administrative messages and event controls. Notably, DEEP does not disclose individual order details, such as non-displayed or reserve orders. It also conveys short sale restriction status, trading status, operational halt status, and security event information. While TOPS focuses specifically on top of the order book, DEEP offers a comprehensive view of the market depth.  DEEP's level of granularity is crucial for quant strategists interested in strategy development and backtesting, because it allows them to study liquidity, latency, other market dynamics, and optimize their strategies. The data from DEEP was used for backtesting. 

**Downloading and Parsing Data Feeds**

I utilized the [IEX Downloader Parser](https://gitlab.engr.illinois.edu/shared_code/iexdownloaderparser)developed by David Lariviere to download and parse PCAP files. 

#### 4.1.2 Futures

NASDAQ, crude oil, and gold Futures data was only used to backtest the taker strategy. I have obtained the historical Futures data through [TradingView](https://tradingview.com), which obtained data directly from CME, NYMEX, and COMEX, respectively.

#### 4.1.3 Crypto

Likewise, crypto data was only used to backtest the taker strategy, and was obtained through TradingView, which obtained data directly from Binance.

### 4.2 Backtesting Period

I have chosen a period from JANUARY 3, 2023 to February 3, 2023 for the US Cash Equities market to highlight a high volatility period in the stock market. This uncertainity and above average volatility was fueled by the rising CPI in USA and the Ukraine-Russia conflict. 

However, due to high cost of accessing high-precision historical market data for Futures and Crypto market, I have chosen the most recent (YTD) data, which I could access for free through TradingView.

## 5. Taker Strategy

### 5.1 Introduction

![taker strategy](./image/taker.png)

Strategy utilizes Renko Chart and Bollinger Bands. Renko Chart was employed to better understand the trend of the market. Shifted Bollinger Bands was utilized to confirm the trend and filter false trend.

* **Buy Signal**: Renko candle breaks above the band

* **Sell Signal**: Renko candle breaks below the band

### 5.2 Parameter Optimization

**List of Parameters**:

* Box size
* Bollinger Bands Length
* Bollinger Bands Standard Deviation
* Bollinger Bands Offset
* Take Profit Number of Boxes
* Stop Loss Number of Boxes

![Parameter Optimization](./taker_strategy/results/parameter_optimization.png)

Figure above visualizes the impact of each parameters to the profit (PNL), Winrate, and Profit Ratio. 

**Box Size**:

The smaller the box size, the greater the PNL and Winrate, but the Profit Ratio remained similar. Explanation for such could be that as the box size gets smaller, the greater the precision of the trend, leading to better entry prices (could enter the trend much earlier). 

**BB Length**:

The smaller the BB length, the greater the PNL and Winrate, but the Profit Ratio remained similar. Explanation for such could be that as the length gets shorter, the strategy could adapt to the trend shift much quicker.

**BB SD**:

The smaller the BB SD, the greater the Winrate, but the PNL and Profit Ratio remained similar.

**BB Offset**:

The offset did not seem to have much impact on PNL, Winrate, and Profit Ratio.

**TP/SL**:

Implementing take profit and stop loss was more beneficial in terms of PNL and winrate as it limits the loss. However, in return, profit ratio was not as good since it takes profit much earlier (does not ride the trend till the end).
<br><br/>

Overall, the box plot above suggests that **Box Size** is the most important parameter.

### 5.3 Backtest

#### 5.3.1 US Cash Equities
![optimized QQQ](./taker_strategy/results/QQQ_optimized.png)

Figure above shows the Monte Carlo simulation of real trade data and simulated quotes of the optimized parameters on QQQ. In all 10 backtests, the strategy was profitable.

#### 5.3.2 Futures

Backtesting on Futures has been done through PineScript language.


#### 5.3.2.1 NQ

![NQ backtest](./taker_strategy/results/NQ.png)

Figure above shows the YTD backtest result with following parameters:

* **Trade Size**: 1 contract
* **Box Size**: 25
* **BB Length**: 5
* **BB SD**: 1
* **BB Offset**: 2
* **TP/SL**: (5,2)

**Statistics**

* **PNL**: $88,740
* **Winrate**: 42.14%
* **Profit Ratio**: 1.258
* **Sharpe Ratio**: 2.495

#### 5.3.2.2 CL

![CL backtest](./taker_strategy/results/CL.png)

Figure above shows the YTD backtest result with following parameters:

* **Trade Size**: 1 contract
* **Box Size**: 0.3
* **BB Length**: 5
* **BB SD**: 1
* **BB Offset**: 2
* **TP/SL**: (5,2)

**Statistics**

* **PNL**: $15,330
* **Winrate**: 40.24%
* **Profit Ratio**: 1.171
* **Sharpe Ratio**: 0.202

#### 5.3.2.3 GC

![GC backtest](./taker_strategy/results/GC.png)

Figure above shows the YTD backtest result with following parameters:

* **Trade Size**: 1 contract
* **Box Size**: 0.5
* **BB Length**: 5
* **BB SD**: 1
* **BB Offset**: 2
* **TP/SL**: (5,2)

**Statistics**

* **PNL**: $82,760
* **Winrate**: 40.67%
* **Profit Ratio**: 1.144
* **Sharpe Ratio**: 0.8

#### 5.3.3 Crypto

#### 5.3.3.1 BTC/USDT

![BTC/USDT backtest](./taker_strategy/results/BTCUSDT.png)

Figure above shows the YTD backtest result with following parameters:

* **Trade Size**: 1 BTC
* **Box Size**: 100
* **BB Length**: 5
* **BB SD**: 1
* **BB Offset**: 2
* **TP/SL**: (5,2)

**Statistics**

* **PNL**: $69,322.10
* **Winrate**: 40.64%
* **Profit Ratio**: 1.103
* **Sharpe Ratio**: 1.175

#### 5.3.3.2 ETH/USDT

![ETH/USDT backtest](./taker_strategy/results/ETHUSDT.png)

Figure above shows the YTD backtest result with following parameters:

* **Trade Size**: 1 ETH
* **Box Size**: 5
* **BB Length**: 5
* **BB SD**: 1
* **BB Offset**: 2
* **TP/SL**: (5,2)

**Statistics**

* **PNL**: $3,214.19
* **Winrate**: 40.87%
* **Profit Ratio**: 1.073
* **Sharpe Ratio**: -1.158

### 5.4 Conclusion / Future Work

The strategy seems to be **highly** parameter-dependent. However, once the "good" parameter has been chosen, the strategy has been profitable. This indicates that it is crucial to fine-tune the parameters, but once done, it will provide a promising return. Further research needs to be done to find an equation to compute the best box size given the price, tick size, and the daily Average True Range (ATR) of a given security.

## 6. Maker Strategy

### 6.1 Introduction

![maker strategy](./image/maker.png)

Market-making strategy takes advantage of increased volume when the price approaches the major level, as can be seen from the figure above.

Following are the major levels used throughout the strategy:

* Level Increment (If level increment is 0.25, then the levels are 100, 100.25, 100.50, ...)
* Support/Resistance
* Previous day Open, High, Low, Close, and Middle Price (High+Low/2)
* Current day Open, High, Low, and Middle Price
* Pivot Points
* Demark Levels

When certain number of levels overlap within some threshold, then the strategy performs market-making.

### 6.2 Clustering

Upon computing swing high and low, clustering algorithm has been implemented to compute the support and resistance level. Figure below shows all the swing high and low levels before clustering. 

![before clustering](./maker_strategy/results/clustering/QQQ_20191001_20191101_10_swing_high_low_without_clustering.png)

**Centroid Clustering**

 This technique uses centroids, which represent the center of each cluster, to minimize the sum of the distances between the data points and their corresponding cluster centroids

![centroid clustering](./maker_strategy/results/clustering/QQQ_20191001_20191101_10_swing_high_low_daily_centroid_clustering.png)

Though there are less "clusters," there still are overly dense regions.

**DBSCAN Clustering**

DBSCAN (Density-Based Spatial Clustering of Applications with Noise) is a powerful clustering algorithm used in machine learning to partition data into clusters based on their proximity to other points.

![DBSCAN clustering](./maker_strategy/results/clustering/QQQ_20191001_20191101_10_swing_high_low_daily_DBSCAN_clustering.png)

DBSCAN method has clustered more effectively.

***DBSCAN clustering method will be utilized to compute the support and resistance levels from swing high and lows.***

### 6.3 Backtest

Backtesting has been performed from January 3, 2023 to February 3, 2023 on the US Cash Equities market, specifically SPY.

**Parameters**:

* **Trade Size**: 10 shares per trade update
* **Major Level Increment**: 1.0
* **Zone Width**: 0.08
* **Overlap Required**: 2
* **Swing High-Low Length**: 24

**Result**

![market making result](./maker_strategy/results/SPY_marketmaking.png)

**Statistics**:

* **PNL**: $46,170.40
* **Win Rate**: 51.46%
* **Profit Ratio**: 0.9477
* **Sharpe Ratio**:6.74e-4

The strategy was quite profitable, though win rate and the ratios were under-par as well as occasionally experiencing spikes in pnl (both upwards and downwards). This could be due to submitting orders whenever the trade update has been called and those pile orders get filled at once; I assumed that to provide the liquidity to the market, market-making orders should be submited whenever the liquidity has been removed from the orderbook (trades).

![correlation](./maker_strategy/results/correlation.png)

Figure above compares the daily PNL of the maker strategy and SPY daily percentage change. Interestingly, two lines in the plot seems relatively correlated with correlation coefficient of **0.57**. This highlights potential improvement that could be made utilizing machine learning: ML model predicts the percentage change, and market-making position size could be adjusted accordingly to increase position size during profit-likely days.

### 6.5 Conclusion / Future Work

Due to IEX exchange's **rebate** for posting liquidity on the orderbook, a strategy that does not lose money is still considered a profitable strategy. The strategy returned a optimal result on a given backtesting window. Though the strategy cumulative PNL was quite oscillating, but still had a positive trend/slope. As mentioned earlier, area of potential improvement is quite clear: implementing a machine learning model that predicts the market movement, which would change the trade size.

## 7. Reflection

1. **What did you learn as a result of doing your project?**

	* Deeper understanding in quant/algorithmic strategy generation process

	* **Think in terms of retail traders when market-making**

		* Identify price levels, which the retail traders are likely to be entering the trade, beforehand 

		* May be counterintuitive, but worked for me

2. **If you had a time machine and could go back to the beginning, what would you have done differently?**

    If I could go back to the beginning of the project, I would like to focus more (or even completely) on the market-making strategy. I have spent first half of the semester developing the taker strategy, and the second half for maker strategy. Personally, generating profitable market-making strategy was more challenging and novel for me, which makes me more eager and enthusiastic.

3. **If you were to continue working on this project, what would you continue to do to improve it, how, and why?**

    If I were to continue working on this project, I would like to introduce machine learning models to both maker and taker strategies. For maker strategy, ML model will predict the security's percentage change and adjust the position size accordingly. For taker strategy, ML model will predict the best Renko box size given price, tick size, and the volatility of the security.

4. **What advice do you offer to future students working on their semester long project?**

    I would like to recommend future students to make a great use of Large Language Models (LLM) such as ChatGPT or Copilot to aid the coding process. LLMs have been extremely helpful, acting as my project assitant.

Overall, I am truly proud of my own work of generating profitable strategies for both market-taking and market-making. However, I regret that I was not able to fully focus on this project and give my 100% of the effor due to simultaneously working on a different [market-microstructure project](https://gitlab.engr.illinois.edu/ie421_high_frequency_trading_spring_2024/ie421_hft_spring_2024_group_04/group_04_project). Still, I was able to further self-train the tasks of Quant Developer at the trading firms. 
