/*================================================================================
*     Source: ../RCM/StrategyStudio/examples/strategies/SimpleMomentumStrategy/SimpleMomentumStrategy.cpp
*     Last Update: 2021/04/15 13:55:14
*     Contents:
*     Distribution:
*
*
*     Copyright (c) RCM-X, 2011 - 2021.
*     All rights reserved.
*
*     This software is part of Licensed material, which is the property of RCM-X ("Company"), 
*     and constitutes Confidential Information of the Company.
*     Unauthorized use, modification, duplication or distribution is strictly prohibited by Federal law.
*     No title to or ownership of this software is hereby transferred.
*
*     The software is provided "as is", and in no event shall the Company or any of its affiliates or successors be liable for any 
*     damages, including any lost profits or other incidental or consequential damages relating to the use of this software.       
*     The Company makes no representations or warranties, express or implied, with regards to this software.                        
/*================================================================================*/

#ifdef _WIN32
    #include "stdafx.h"
#endif

#include "ZoneMarketMaking.h"

#include "FillInfo.h"
#include "AllEventMsg.h"
#include "ExecutionTypes.h"
#include <Utilities/Cast.h>
#include <Utilities/utils.h>

#include <cstdlib> 
#include <math.h>
#include <iostream>
#include <cassert>
#include <cmath>
#include <algorithm>

using namespace RCM::StrategyStudio;
using namespace RCM::StrategyStudio::MarketModels;
using namespace RCM::StrategyStudio::Utilities;

using namespace std;
int total_trades = 0;
int buy_trades = 0;
int sell_trades = 0;
int bid_act_count = 0;
int ask_act_count = 0;

ZoneMarketMaking::ZoneMarketMaking(StrategyID strategyID, const std::string& strategyName, const std::string& groupName):
    Strategy(strategyID, strategyName, groupName),
    regime_map_(),    
    position_size_(5),
    constant_fraction_position(1),
    time_interval(1),
    debug_(false),
    i_threshold_min(2),
    i_threshold_max(5),
    target_inventory(0),
    target_time(),
    total_stock_position(0),
    threshold_time(20,59,55),
    crossed_threshold_time(false)
    major_level_increment(0.5),
    zone_width(0.1),
    swing_high_low_length(24),
    num_overlap_required(2),
    tp_tick(4),
    sl_tick(20)
    _ /*< SR, Pivot, Demark */
{
    
    // by default current_candlestick is open 
    current_candlestick.set_open();
    
    // #ifdef ShortWindow
    // #pragma message("ShortWindow given: " ShortWindow)
    // std::string shortWindowStr = ShortWindow;
    // int temp_short_window_size = std::stoi(shortWindowStr);
    // candlestickQueue.set_short_window_size(temp_short_window_size);
    // #endif

    // #ifdef MediumWindow
    // #pragma message("MidWindow given: " MediumWindow)
    // std::string midWindowStr = MediumWindow;
    // int temp_mid_window_size = std::stoi(midWindowStr);
    // candlestickQueue.set_mid_window_size(temp_mid_window_size);
    // #endif

    // #ifdef LongWindow
    // #pragma message("LongWindow given: " LongWindow)
    // std::string longWindowStr = LongWindow;
    // int temp_long_window_size = std::stoi(longWindowStr);
    // candlestickQueue.set_long_window_size(temp_long_window_size);
    // #endif
}

ZoneMarketMaking::~ZoneMarketMaking(){}


void ZoneMarketMaking::OnResetStrategyState()
{
    regime_map_.clear();
}

void ZoneMarketMaking::DefineStrategyParams()
{
    params().CreateParam(CreateStrategyParamArgs("aggressiveness", STRATEGY_PARAM_TYPE_RUNTIME, VALUE_TYPE_DOUBLE, aggressiveness_));
    params().CreateParam(CreateStrategyParamArgs("position_size", STRATEGY_PARAM_TYPE_RUNTIME, VALUE_TYPE_INT, position_size_));
    params().CreateParam(CreateStrategyParamArgs("major_level_increment", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_DOUBLE, major_level_increment));
    params().CreateParam(CreateStrategyParamArgs("zone_width", STRATEGY_PARAM_TYPE_STARTUP, VALUE_TYPE_DOUBLE, zone_width));
    params().CreateParam(CreateStrategyParamArgs("swing_high_low_length", STRATEGY_PARAM_TYPE_RUNTIME, VALUE_TYPE_INT, swing_high_low_length));
    params().CreateParam(CreateStrategyParamArgs("num_overlap_required", STRATEGY_PARAM_TYPE_RUNTIME, VALUE_TYPE_INT, num_overlap_required));
    params().CreateParam(CreateStrategyParamArgs("tp_tick", STRATEGY_PARAM_TYPE_RUNTIME, VALUE_TYPE_INT, tp_tick));
    params().CreateParam(CreateStrategyParamArgs("sl_tick", STRATEGY_PARAM_TYPE_RUNTIME, VALUE_TYPE_INT, sl_tick));
    params().CreateParam(CreateStrategyParamArgs("debug", STRATEGY_PARAM_TYPE_RUNTIME, VALUE_TYPE_BOOL, debug_));
}

void ZoneMarketMaking::DefineStrategyCommands()
{
    commands().AddCommand(StrategyCommand(1, "Reprice Existing Orders"));
    commands().AddCommand(StrategyCommand(2, "Cancel All Orders"));
}

void ZoneMarketMaking::RegisterForStrategyEvents(StrategyEventRegister* eventRegister, DateType currDate)
{    
    for (SymbolSetConstIter it = symbols_begin(); it != symbols_end(); ++it) {
        eventRegister->RegisterForBars(*it, BAR_TYPE_TIME, time_interval);
    }
}

void ZoneMarketMaking::OnDepth(const MarketDepthEventMsg& msg){}

void ZoneMarketMaking::OnTrade(const TradeDataEventMsg& msg)
{
    // std::cout << "On trade called" << std::endl;

    const Instrument& instrument = msg.instrument();
    int num_working_orders = orders().num_working_orders(&instrument);
    int num_of_open_trades = trades().num_open_trades(&instrument);
    int num_of_closed_trades = trades().num_closed_trades(&instrument);
    auto filled_orders = orders().stats().num_orders_filled();
    int position = portfolio().position(&instrument);
    int overlap = 0;
    // std::cout << "checkpoint 0" << std::endl;
    
    // candlestickQueue.print_high_asks();
    // candlestickQueue.print_close_prices();

    current_candlestick.set_close_price(msg.trade().price());

    
    TimeType current_time = msg.source_time();
    DateType current_date = current_time.date();
    TimeType final_threshold_time = TimeType(current_date, threshold_time);

    // std::cout << "Total pnl: " << portfolio().total_pnl() << ", Realized pnl: "<< portfolio().realized_pnl() << ", Unrealized pnl: "<< portfolio().unrealized_pnl() << std::endl;

    // Stop Loss implementation
    if (position != 0 && portfolio().unrealized_pnl() < -std::abs(position)*0.01*sl_tick) {
        trade_actions()->SendCancelAll();
        OrderParams params(instrument,
                       abs(position),
                       instrument.top_quote().bid(),
                       MARKET_CENTER_ID_IEX,
                       (position > 0) ? ORDER_SIDE_SELL : ORDER_SIDE_BUY,
                       ORDER_TIF_DAY,
                       ORDER_TYPE_MARKET);
        trade_actions()->SendNewOrder(params);
    }

    // Check if threshold time has been crossed
    if (current_time > final_threshold_time) {
        this->crossed_threshold_time = true;
    } else {
        this->crossed_threshold_time = false;
    }

    // If past the order close thrashold, cancel and close all trades
    if(crossed_threshold_time) {  
        int current_inventory = portfolio().position(&instrument);

        if (current_inventory > 0) {
            OrderParams params(instrument,
                abs(current_inventory),
                instrument.top_quote().bid(),
                MARKET_CENTER_ID_IEX,
                (current_inventory > 0) ? ORDER_SIDE_SELL : ORDER_SIDE_BUY,
                ORDER_TIF_DAY,
                ORDER_TYPE_MARKET);

            trade_actions()->SendCancelAll();
            trade_actions()->SendNewOrder(params);
                
            } else if (current_inventory < 0) {
                OrderParams params(instrument,
                        abs(current_inventory),
                        instrument.top_quote().ask(),
                        MARKET_CENTER_ID_IEX,
                        (current_inventory > 0) ? ORDER_SIDE_SELL : ORDER_SIDE_BUY,
                        ORDER_TIF_DAY,
                        ORDER_TYPE_MARKET);

                trade_actions()->SendCancelAll();
                trade_actions()->SendNewOrder(params);
            }
    }
    
    // Regular trading hour for the strategy
    else {
        if(candlestickQueue.size() < candlestickQueue.get_window_size()) {
            candlestickQueue.addCandlestick(current_candlestick);
            current_candlestick.reset();
            current_candlestick.set_open_price(msg.trade().price());
            return;
        } else {
            // Check if current price is within the major level zone
            if (std::fmod(msg.trade().price(),major_level_increment) >= major_level_increment - zone_width/2 
                    || std::fmod(msg.trade().price(),major_level_increment) <= zone_width/2) {
                        overlap++;
                    }

            for (auto level : zones) {
                if (std::fmod(msg.trade().price(),level) >= level - zone_width/2 
                    || std::fmod(msg.trade().price(),level) <= level/2) {
                        overlap++;
                    }
            }

            bool candle_is_added = candlestickQueue.addCandlestick(current_candlestick);
            
            if(candle_is_added) {
                // predictive signal - If BL/CL crossover predicted, send signal
                bool buy_signal = overlap >= num_overlap_required && msg.trade().price() <= (candlestickQueue.highest_high() + candlestick_Queue.lowest_low())/2;
                bool sell_signal = overlap >= num_overlap_required && msg.trade().price() > (candlestickQueue.highest_high() + candlestick_Queue.lowest_low())/2;

                // Close position before entering a next trade
                if (portfolio().position(&instrument) != 0) {
                    OrderParams params(instrument,
                       abs(portfolio().position(&instrument)),
                       instrument.top_quote().bid(),
                       MARKET_CENTER_ID_IEX,
                       (portfolio().position(&instrument) > 0) ? ORDER_SIDE_SELL : ORDER_SIDE_BUY,
                       ORDER_TIF_DAY,
                       ORDER_TYPE_MARKET);
                    trade_actions()->SendNewOrder(params);
                }

                if(buy_signal)
                {
                    trade_actions()->SendCancelAll();
                    
                    this->MakeOrder(instrument,-1);
                }
                else if (sell_signal)
                {
                    trade_actions()->SendCancelAll();

                    this->MakeOrder(instrument,1);
                }
            }
        }
    } 
}

void ZoneMarketMaking::OnBar(const BarEventMsg& msg)
{ }

void ZoneMarketMaking::OnQuote(const QuoteEventMsg& msg) {}

double ZoneMarketMaking::CalculateInventoryGap(const Instrument* instrument)
{
    int current_inventory = portfolio().position(instrument);
    return (target_inventory - current_inventory);
}

int ZoneMarketMaking::CalculateTargetInventory(const Instrument* instrument,double indicator)
{
    // double indicator_scale = min(indicator/instrument->min_tick_size(),double(i_threshold_max))/(i_threshold_max - i_threshold_min);
    // int target = int(indicator_scale*position_size_);
    // return target;
}

void ZoneMarketMaking::OnOrderUpdate(const OrderUpdateEventMsg& msg) {}

double ZoneMarketMaking::TimeDifferenceInMicroseconds(TimeType time1, TimeType time2)
{
    // Convert the TimeType objects to microsecond precision if needed
    TimeType t1_microsec = boost::posix_time::time_from_string(boost::posix_time::to_iso_extended_string(time1) + ".000");
    TimeType t2_microsec = boost::posix_time::time_from_string(boost::posix_time::to_iso_extended_string(time2) + ".000");

    // Calculate the difference in microseconds
    boost::posix_time::time_duration diff = t2_microsec - t1_microsec;
    return diff.total_microseconds();
}

double ZoneMarketMaking::NormalisedTime(TimeType time1)
{
    double t1 = TimeDifferenceInMicroseconds(time1, target_time);
    return t1/time_interval;
}

void ZoneMarketMaking::MakeOrder(const Instrument& instrument, int action_type)
{
    // std::cout << "Total pnl: " << portfolio().total_pnl() << std::endl;

    // std::cout << "Called makeorder" << std::endl;
    int current_position = portfolio().position(&instrument);
    auto all_buy_orders = orders().stats().long_working_order_size();
    auto all_sell_orders = orders().stats().short_working_order_size();
    // auto long_order_pending = orders().stats().long_working_order_size();
    auto filled_orders = orders().stats().num_orders_filled();
    
    int num_working_orders = orders().num_working_orders(&instrument);
    int trade_size = action_type == 1 ? position_size_ * constant_fraction_position : -position_size_ * constant_fraction_position;
    
    

    if (action_type == 1)
        {
            SendOrder(&instrument, trade_size);
            // std::cout << "filled orders: " << filled_orders << std::endl;
            SendOrder(&instrument, -trade_size, false);
            bid_act_count += 1;
        }
    else
    {
        SendOrder(&instrument, trade_size, false);
        SendOrder(&instrument, -trade_size);
        ask_act_count += 1;
    }    
   

}

void ZoneMarketMaking::AdjustPortfolio(const Instrument* instrument, int desired_position)
{
    // total_stock_position = portfolio().position(instrument);
    // if (total_stock_position == 0)
    // if (trade_size != 0) {
    //     // if we're not working an order for the instrument already, place a new order
    //     if (orders().num_working_orders(instrument) == 0) {
    //         SendOrder(instrument, trade_size);
    //     } else {  
    //         // otherwise find the order and cancel it if we're now trying to trade in the other direction
    //         const Order* order = *orders().working_orders_begin(instrument);
    //         if (order && ((IsBuySide(order->order_side()) && trade_size < 0) || 
    //                      ((IsSellSide(order->order_side()) && trade_size > 0)))) {
    //             trade_actions()->SendCancelOrder(order->order_id());
    //             // we're avoiding sending out a new order for the other side immediately to simplify the logic to the case where we're only tracking one order per instrument at any given time
    //         }
    //     }
    // }
}

void ZoneMarketMaking::SendOrder(const Instrument* instrument, int trade_size, bool flag = true)
{
      
    if (!instrument->top_quote().ask_side().IsValid() || !instrument->top_quote().ask_side().IsValid()) {
            std::stringstream ss;
            ss << "Skipping trade due to lack of two sided quote"; 
            logger().LogToClient(LOGLEVEL_DEBUG, ss.str());
            return;
        }
    const Trade& last_trade = instrument->last_trade();


    // bool is_ask_side =last_trade.is_ask_side();
    // std::cout << "Send Order called" << std::endl;
    double price;


    //For each buy or sell signal, flag provides the information whether we are placing the main order (true) or the corresponding directional trade (false)
    if (flag)
    {
        price = trade_size > 0 ? instrument->top_quote().bid(): instrument->top_quote().ask();
        // std::cout << "price" << price << std::endl;
    }
    else
    {
        price = trade_size > 0 ? instrument->top_quote().bid() - 0.01: instrument->top_quote().ask() + 0.01;
        // std::cout << "agg_bid: " << agg_bid << std::endl;
    }

    OrderParams params(*instrument,
                       abs(trade_size),
                       price,
                       MARKET_CENTER_ID_IEX,
                       (trade_size > 0) ? ORDER_SIDE_BUY : ORDER_SIDE_SELL,
                       ORDER_TIF_DAY,
                       ORDER_TYPE_LIMIT);

    double std_current_closing_prices = candlestickQueue.get_standard_deviation_short_window();
        
    trade_actions()->SendNewOrder(params);
}

void ZoneMarketMaking::RepriceAll()
{
    for (IOrderTracker::WorkingOrdersConstIter ordit = orders().working_orders_begin(); ordit != orders().working_orders_end(); ++ordit) {
        Reprice(*ordit);
    }
}

void ZoneMarketMaking::Reprice(Order* order)
{
    OrderParams params = order->params();
    params.price = (order->order_side() == ORDER_SIDE_BUY) ? order->instrument()->top_quote().bid() + aggressiveness_ : order->instrument()->top_quote().ask() - aggressiveness_;
    trade_actions()->SendCancelReplaceOrder(order->order_id(), params);
}

void ZoneMarketMaking::OnStrategyCommand(const StrategyCommandEventMsg& msg)
{
    switch (msg.command_id()) {
        case 1:
            RepriceAll();
            break;
        case 2:
            trade_actions()->SendCancelAll();
            break;
        default:
            logger().LogToClient(LOGLEVEL_DEBUG, "Unknown strategy command received");
            break;
    }
}

void ZoneMarketMaking::OnParamChanged(StrategyParam& param)
{    
    if (param.param_name() == "aggressiveness") {
        if (!param.Get(&aggressiveness_))
            throw StrategyStudioException("Could not get aggressiveness");
    } else if (param.param_name() == "position_size") {
        if (!param.Get(&position_size_))
            throw StrategyStudioException("Could not get position_size");
    } else if (param.param_name() == "major_level_increment") {
        if (!param.Get(&major_level_increment))
            throw StrategyStudioException("Could not major_level_increment");
    } else if (param.param_name() == "zone_width") {
        if (!param.Get(&zone_width))
            throw StrategyStudioException("Could not get zone_width");
    } else if (param.param_name() == "swing_high_low_length") {
        if (!param.Get(&swing_high_low_length))
            throw StrategyStudioException("Could not get swing_high_low_length");
    } else if (param.param_name() == "num_overlap_required") {
        if (!param.Get(&num_overlap_required))
            throw StrategyStudioException("Could not num_overlap_required");
    } else if (param.param_name() == "tp_tick") {
        if (!param.Get(&tp_tick))
            throw StrategyStudioException("Could not get tp_tick");
    } else if (param.param_name() == "sl_tick") {
        if (!param.Get(&sl_tick))
            throw StrategyStudioException("Could not get sl_tick");
    } else if (param.param_name() == "debug") {
        if (!param.Get(&debug))
            throw StrategyStudioException("Could not get debug");
    }
}

