import numpy as np
import os
import pandas as pd
import sys
import matplotlib.pyplot as plt
import plotly.graph_objects as go
from maker_strategy import data_parser as dp

pd.set_option('display.max_rows', None)
pd.options.mode.chained_assignment = None  # default='warn'


def is_within_error(number_list, target_float, error, num_required):
    """
    Checks if the target_float is within the specified absolute error of any number in the list.

    Args:
        number_list (list): List of floats.
        target_float (float): The target float to check.
        error (float): Absolute error threshold.

    Returns:
        bool: True if target_float is within error of any number in the list, False otherwise.
    """
    count_within_error = 0
    for num in number_list:
        if abs(target_float - num) <= error:
            count_within_error += 1
            if count_within_error >= num_required:
                return True
    return False


def run_backtest(ticker, start_date, end_date, strategy_parameter, **kwargs):
    # Parameter set
    capital = 1000000   # USD
    position_size = 3   # %
    level_increment, zone_width, swing_high_low_length, num_overlap_required, tp_tick, sl_tick = strategy_parameter

    sr_level = kwargs.get("sr_level", [])
    use_prev_day = kwargs.get("use_prev_day", False)
    use_curr_day = kwargs.get("use_curr_day", False)
    use_pivot = kwargs.get("use_pivot", False)
    use_demark = kwargs.get("use_demark", False)

    # Parse data and add simulated quotes
    df = dp.read_trade_tick_data(ticker, start_date, end_date, include_premarket=True)
    df = dp.add_simulated_bid_ask(df)

    position = 0
    order_num = 1
    working_order = []  # [order_num, size, [buy_order_price, filled?], [sell_order_price, filled?], [buy_sl_price, filled?], [sell_sl_price, filled?]]
    trades_list = []    # [Open time, Close time, Open position, open price, close price, pnl]
    pnl_list = []

    candlestick_df = pd.DataFrame(columns=["time", "open", "high", "low", "close", "volume"])
    next_time = None
    day_open, day_high, day_low, day_close, day_mid_price = -1, -1, -1, -1, -1
    prev_open, prev_day_high, prev_day_low, prev_close, prev_day_mid_price = -1, -1, -1, -1, -1
    pivot_point, s1, s2, r1, r2 = -1, -1, -1, -1, -1
    demark, dh, dl = -1, -1, -1
    
    swing_high_low = []
    swing_high_low_clusters = []

    levels_list = sr_level[:]

    # Start backtesting
    for row in range(len(df)):
        if row == 0 or df["Collection Time"][row].day != df["Collection Time"][row-1].day:  # Check if new day
            position = 0

            # Update previous day and reset current day variables
            prev_day_high = day_high
            prev_day_low = day_low
            prev_open = day_open
            prev_close = day_close
            prev_day_mid_price = (prev_day_high + prev_day_low)/2
            day_high, day_low, day_open, day_close = -1, -1, -1, -1

            # Compute pivot point
            pivot_point = (prev_day_high+prev_day_low+prev_close)/3
            s1 = pivot_point * 2 - prev_day_high
            s2 = pivot_point - (prev_day_high - prev_day_low)
            r1 = pivot_point * 2 - prev_day_low
            r2 = pivot_point + (prev_day_high - prev_day_low)

            # Compute demark
            if prev_close > prev_open:
                demark = (prev_day_high+prev_day_low+prev_close+prev_day_high)/4
            elif prev_close < prev_open:
                demark = (prev_day_high+prev_day_low+prev_close+prev_day_low)/4
            else:
                demark = (prev_day_high+prev_day_low+prev_close+prev_close)/4

            dh = demark * 2 - prev_day_low
            dl = demark * 2 - prev_day_high

            if row != 0 and len(swing_high_low) != 0:
                swing_high_low_clusters = []
                points_sorted = sorted(swing_high_low)
                curr_point = points_sorted[0]
                curr_cluster = [curr_point]
                for point in points_sorted[1:]:
                    if point <= curr_point + zone_width/2:
                        curr_cluster.append(point)
                    else:
                        swing_high_low_clusters.append(curr_cluster)
                        curr_cluster = [point]
                    curr_point = point
                swing_high_low_clusters.append(curr_cluster)

            # Update levels list
            levels_list = sr_level[:]

            if use_prev_day:
                levels_list += [prev_open, prev_day_high, prev_day_low, prev_close, prev_day_mid_price]

            if use_curr_day:
                levels_list += [day_open, day_mid_price]    # Not using day high and low prices since it would result in a great momentum with liquidity void

            if use_pivot:
                levels_list += [pivot_point, s1, s2, r1, r2]

            if use_demark:
                levels_list += [demark, dh, dl]


        if row == len(df) - 1 or df["Collection Time"][row].day != df["Collection Time"][row+1].day: # Check if last trade of the day (only for getting close price)
            day_close = df["Price"][row]

        if df["Tick Type"][row] == "Q":     # Submit orders per strategy
            last_traded_price = round(df["Price"][row-1], 2)

            if last_traded_price == 0:
                continue

            if len(working_order) == 0 and is_within_error(levels_list + [round(np.mean(i),2) for i in swing_high_low_clusters] + [round(last_traded_price // level_increment * level_increment, 2), round(last_traded_price // level_increment * (level_increment + 1), 2)], 
                               last_traded_price, zone_width/2, num_overlap_required):
                # send market-making order
                position_to_trade = int((capital * 0.01 * position_size) // last_traded_price)

                bp, sp = 0,0

                if tp_tick % 2 == 0:
                    bp = round(last_traded_price - 0.01 * tp_tick / 2, 2)
                    sp = round(last_traded_price + 0.01 * tp_tick / 2, 2)
                else:
                    if len(swing_high_low) == 0 or swing_high_low[-1] > last_traded_price:
                        bp = round(last_traded_price - 0.01 * (tp_tick//2),2)
                        sp = round(last_traded_price + 0.01 * (tp_tick//2 + 1),2)
                    else:
                        bp = round(last_traded_price - 0.01 * (tp_tick//2 + 1),2)
                        sp = round(last_traded_price + 0.01 * (tp_tick//2),2)

                working_order.append([order_num, position_to_trade, [bp, 0], [sp, 0], [round(bp - 0.01*sl_tick,2), 0], [round(bp + 0.01*sl_tick,2), 0]])

            if len(working_order) == 1:
                # Check if buy order could be filled
                if working_order[0][2][1] == 0 and df["Ask Price"][row] <= working_order[0][2][0] and working_order[0][2][1] + working_order[0][3][1] + working_order[0][4][1] + working_order[0][5][1] < 2:
                    # [Open time, Close time, Open position, open price, close price, pnl]
                    working_order[0][2][1] = 1
                    position += int(working_order[0][1])

                    if len(trades_list) == 0 or not np.isnan(trades_list[-1][4]):
                        trades_list.append([df["Collection Time"][row], "", working_order[0][1], round(df["Ask Price"][row],5), np.nan, np.nan])

                    else:
                        trades_list[-1][1] = df["Collection Time"][row]
                        trades_list[-1][4] = round(df["Ask Price"][row],5)
                        trades_list[-1][5] = round((trades_list[-1][4] - trades_list[-1][3]) * trades_list[-1][2],5)
                        pnl_list.append(trades_list[-1][5])
                        capital += pnl_list[-1]


                # Check if sell order could be filled
                if working_order[0][3][1] == 0 and df["Bid Price"][row] >= working_order[0][3][0] and working_order[0][2][1] + working_order[0][3][1] + working_order[0][4][1] + working_order[0][5][1] < 2:
                    # [Open time, Close time, Open position, open price, close price, pnl]
                    working_order[0][3][1] = 1
                    position -= int(working_order[0][1])
 
                    if len(trades_list) == 0 or not np.isnan(trades_list[-1][4]):
                        trades_list.append([df["Collection Time"][row], "", -working_order[0][1], round(df["Bid Price"][row],5), np.nan, np.nan])

                    else:
                        trades_list[-1][1] = df["Collection Time"][row]
                        trades_list[-1][4] = round(df["Bid Price"][row],5)
                        trades_list[-1][5] = round((trades_list[-1][4] - trades_list[-1][3]) * trades_list[-1][2],5)
                        pnl_list.append(trades_list[-1][5])
                        capital += pnl_list[-1]


                # Check if buy sl could be filled
                if working_order[0][2][1] == 1 and working_order[0][4][1] == 0 and df["Bid Price"][row] <= working_order[0][4][0] and working_order[0][2][1] + working_order[0][3][1] + working_order[0][4][1] + working_order[0][5][1] < 2:
                    # [Open time, Close time, Open position, open price, close price, pnl]
                    working_order[0][4][1] = 1
                    position -= int(working_order[0][1])

                    trades_list[-1][1] = df["Collection Time"][row]
                    trades_list[-1][4] = round(df["Bid Price"][row],5)
                    trades_list[-1][5] = round((trades_list[-1][4] - trades_list[-1][3]) * trades_list[-1][2],5)
                    pnl_list.append(trades_list[-1][5])
                    capital += pnl_list[-1]


                # Check if sell sl could be filled
                if len(working_order) == 1 and working_order[0][3][1] == 1 and working_order[0][5][1] == 0 and df["Ask Price"][row] >= working_order[0][5][0] and working_order[0][2][1] + working_order[0][3][1] + working_order[0][4][1] + working_order[0][5][1] < 2:
                    # [Open time, Close time, Open position, open price, close price, pnl]
                    working_order[0][5][1] = 1
                    position += int(working_order[0][1])

                    trades_list[-1][1] = df["Collection Time"][row]
                    trades_list[-1][4] = round(df["Ask Price"][row],5)
                    trades_list[-1][5] = round((trades_list[-1][4] - trades_list[-1][3]) * trades_list[-1][2],5)
                    pnl_list.append(trades_list[-1][5])
                    capital += pnl_list[-1]

            
                # If two trades are executed, remove from working order
                if working_order[0][2][1] + working_order[0][3][1] + working_order[0][4][1] + working_order[0][5][1] == 2:
                    working_order.pop(0)

                

        elif df["Tick Type"][row] == "T":   # Update indicators
            price = df["Price"][row]

            # Update day high
            if day_high == -1:
                day_high = price
            elif price > day_high:
                day_high = price

            # Update day low
            if day_low == -1:
                day_low = price
            elif price < day_low:
                day_low = price

            day_mid_price = (day_high+day_low)/2

            # Update 5 minute candlestick dataframe
            if len(candlestick_df) == 0:
                candlestick_df.loc[0] = [df["Collection Time"][row].floor("5T"), price, price, price, price, df["Size"][row]]
                next_time = candlestick_df["time"][0] + pd.Timedelta(minutes=5)
                day_open = price

            if df["Collection Time"][row] < next_time:
                if price > candlestick_df["high"][len(candlestick_df)-1]:
                    candlestick_df["high"][len(candlestick_df)-1] = price

                if price < candlestick_df["low"][len(candlestick_df)-1]:
                    candlestick_df["low"][len(candlestick_df)-1] = price

                candlestick_df["close"][len(candlestick_df)-1] = price
                candlestick_df["volume"][len(candlestick_df)-1] += df["Size"][row]

            else:
                 # Compute swing high and low
                if len(candlestick_df) >= swing_high_low_length * 2 + 1:
                    high_to_check = candlestick_df["high"][len(candlestick_df)-1-swing_high_low_length]
                    low_to_check = candlestick_df["low"][len(candlestick_df)-1-swing_high_low_length]

                    if high_to_check == candlestick_df["high"][len(candlestick_df)-1-2*swing_high_low_length:len(candlestick_df)-1].max():
                        swing_high_low.append(high_to_check)

                    if low_to_check == candlestick_df["low"][len(candlestick_df)-1-2*swing_high_low_length:len(candlestick_df)-1].min():
                        swing_high_low.append(low_to_check)

                # Add new candlestick
                candlestick_df.loc[len(candlestick_df)] = [df["Collection Time"][row].floor("5T"), price, price, price, price, df["Size"][row]]
                next_time = candlestick_df["time"][len(candlestick_df)-1] + pd.Timedelta(minutes=5)
                

        else:
            print(f"Invalid backtest data provided with tick type {df['Tick Type'][row]}")
            return
        
        # Close any open position at the end of the day
        if len(trades_list) == 0:
            continue

        if (row == len(df) - 1 or df["Collection Time"][row].day != df["Collection Time"][row+1].day) and position != 0:
            year_ = df["Collection Time"][row].year
            month_ = df["Collection Time"][row].month
            day_ = df["Collection Time"][row].day
            trades_list[-1][1] = pd.Timestamp(year=year_, month=month_, day=day_, hour=20, minute=00, second=00)

            if position > 0:    # close long position
                trades_list[-1][4] = df["Bid Price"][row-1]

            elif position < 0:
                trades_list[-1][4] = df["Ask Price"][row-1]

            trades_list[-1][5] = (trades_list[-1][4] - trades_list[-1][3]) * trades_list[-1][2]
            pnl_list.append(trades_list[-1][5])
            capital += pnl_list[-1]
            position = 0


    # # Create a candlestick chart
    # fig = go.Figure(data=[go.Candlestick(x=candlestick_df['time'],
    #                 open=candlestick_df['open'],
    #                 high=candlestick_df['high'],
    #                 low=candlestick_df['low'],
    #                 close=candlestick_df['close'])])

    # # Customize the chart layout
    # fig.update_layout(
    #     title='Candlestick Chart',
    #     xaxis_title='Time',
    #     yaxis_title='Price',
    #     xaxis_rangeslider_visible=False,
    #     xaxis=dict(
    #         type='category',  # Set x-axis type to category
    #         rangebreaks=[dict(values=pd.date_range(start=candlestick_df['time'].min(), end=candlestick_df['time'].max(), freq='D'))]  # Hide weekends
    #     )
    # )

    # for level in swing_high_low:
    #     fig.add_hline(np.mean(level), candlestick_df['time'][0], exclude_empty_subplots=True)


    # # Show the chart
    # fig.show()


    cumulative_pnl = []
    pnl_sum = 0

    for pnl in pnl_list:
        pnl_sum += pnl
        cumulative_pnl.append(pnl_sum)

    # Calculate winrate (percentage of positive PnL trades)
    num_winning_trades = sum(pnl > 0 for pnl in pnl_list)
    total_trades = len(pnl_list)
    winrate = 0

    if total_trades != 0:
        winrate = num_winning_trades / total_trades

    # Calculate average win and average loss (including break-even trades)
    positive_pnls = [pnl for pnl in pnl_list if pnl >= 0]
    negative_pnls = [pnl for pnl in pnl_list if pnl < 0]
    avg_win = sum(positive_pnls) / len(positive_pnls) if positive_pnls else 0
    avg_loss = sum(negative_pnls) / len(negative_pnls) if negative_pnls else 0

    # Calculate profit ratio (average win / average loss)
    profit_ratio = avg_win / abs(avg_loss) if avg_loss != 0 else float('inf')


    return trades_list, pnl_list, cumulative_pnl, winrate, profit_ratio
