import backtest
import matplotlib.pyplot as plt
import montecarlo

"""
Configuration
"""
# Backtest
ticker = "SPY"
start_date = "20191001"
end_date = "20191101"
num_simulation = 10
path = f"./Python/maker-strategy/results/{ticker}_{start_date}_{end_date}_result.csv"

# Strategy
major_level_increment = [0.5, 1]    # If increment = 0.5, then 187, 187.5, 188 are all major SR levels
zone_width = [0.05, 0.1]
swing_high_low_length = [12, 24]
num_overlap_required = [1, 2, 3]
tp = [2, 4]
sl = [4, 10, 20]


strategy_options = []

for i in major_level_increment:
    for j in zone_width:
        for k in swing_high_low_length:
            for l in num_overlap_required:
                for m in tp:
                    for n in sl:
                        strategy_options.append([i,j,k,l,m,n])

"""
Run Monte Carlo Simulation
"""
pnl_summary, cumulative_pnl_summary, winrate_summary, profit_ratio_summary = montecarlo.perfrom_montecarlo_simulation(ticker, start_date, end_date, 
                    [1, 0.1, 24, 3, 4, 10], num_simulation)

# montecarlo.perfrom_montecarlo_simulation_with_strategy_options(ticker, start_date, end_date, strategy_options, num_simulation, path)


"""
Plot
"""
# plt.figure(figsize=(10, 6))
# for i, sim in enumerate(cumulative_pnl_summary):
#     plt.plot(sim, label=f'Simulation {i+1}')

# plt.xlabel('Trade number')
# plt.ylabel('Cumulative PnL')
# plt.title('Monte Carlo Simulations')
# plt.grid(True)
# plt.legend()
# plt.show()


