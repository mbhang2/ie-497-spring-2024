import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import FactorAnalysis

factor_name = ["Box Size", "BB Length", "SD", "BB Offset", "TP/SL"]
factor_level = [[0.05, 0.1], [5, 10], [1,2], [0,2], ["No", "Yes (5,2)"]]
df = pd.read_csv("./Python/taker-strategy/results/QQQ_20191001_20191101_result.csv")


box_size_vs_pnl = [[[],[]], [[],[]], [[],[]]]       # (pnl/winrate/pr) x (low/high) x instance
bb_length_vs_pnl = [[[],[]], [[],[]], [[],[]]]
sd_vs_pnl = [[[],[]], [[],[]], [[],[]]]
bb_offset_vs_pnl = [[[],[]], [[],[]], [[],[]]]
tp_sl_vs_pnl = [[[],[]], [[],[]], [[],[]]]

for i in range(len(df)):
    # Box size
    if df["Box Size"][i] == 0.05:
        box_size_vs_pnl[0][0].append(df["Avg Cumulative Pnl"][i])
        box_size_vs_pnl[1][0].append(df["Avg Winrate"][i])
        box_size_vs_pnl[2][0].append(df["Avg Profit Ratio"][i])

    else:
        box_size_vs_pnl[0][1].append(df["Avg Cumulative Pnl"][i])
        box_size_vs_pnl[1][1].append(df["Avg Winrate"][i])
        box_size_vs_pnl[2][1].append(df["Avg Profit Ratio"][i])

    # BB length
    if df["BB Length"][i] == 5:
        bb_length_vs_pnl[0][0].append(df["Avg Cumulative Pnl"][i])
        bb_length_vs_pnl[1][0].append(df["Avg Winrate"][i])
        bb_length_vs_pnl[2][0].append(df["Avg Profit Ratio"][i])

    else:
        bb_length_vs_pnl[0][1].append(df["Avg Cumulative Pnl"][i])
        bb_length_vs_pnl[1][1].append(df["Avg Winrate"][i])
        bb_length_vs_pnl[2][1].append(df["Avg Profit Ratio"][i])

    # Standard deviation
    if df["SD"][i] == 1:
        sd_vs_pnl[0][0].append(df["Avg Cumulative Pnl"][i])
        sd_vs_pnl[1][0].append(df["Avg Winrate"][i])
        sd_vs_pnl[2][0].append(df["Avg Profit Ratio"][i])

    else:
        sd_vs_pnl[0][1].append(df["Avg Cumulative Pnl"][i])
        sd_vs_pnl[1][1].append(df["Avg Winrate"][i])
        sd_vs_pnl[2][1].append(df["Avg Profit Ratio"][i])

    # BB offset
    if df["BB Offset"][i] == 0:
        bb_offset_vs_pnl[0][0].append(df["Avg Cumulative Pnl"][i])
        bb_offset_vs_pnl[1][0].append(df["Avg Winrate"][i])
        bb_offset_vs_pnl[2][0].append(df["Avg Profit Ratio"][i])

    else:
        bb_offset_vs_pnl[0][1].append(df["Avg Cumulative Pnl"][i])
        bb_offset_vs_pnl[1][1].append(df["Avg Winrate"][i])
        bb_offset_vs_pnl[2][1].append(df["Avg Profit Ratio"][i])

    # TP/SL
    if df["TP"][i] == -1:
        tp_sl_vs_pnl[0][0].append(df["Avg Cumulative Pnl"][i])
        tp_sl_vs_pnl[1][0].append(df["Avg Winrate"][i])
        tp_sl_vs_pnl[2][0].append(df["Avg Profit Ratio"][i])

    else:
        tp_sl_vs_pnl[0][1].append(df["Avg Cumulative Pnl"][i])
        tp_sl_vs_pnl[1][1].append(df["Avg Winrate"][i])
        tp_sl_vs_pnl[2][1].append(df["Avg Profit Ratio"][i])

factor_list = [box_size_vs_pnl, bb_length_vs_pnl, sd_vs_pnl, bb_offset_vs_pnl, tp_sl_vs_pnl]

fig, axs = plt.subplots(3,5, figsize=(15,9))

for factor_num in range(len(factor_list)):
    # Extract data for the current factor
    pnl_high, pnl_low = factor_list[factor_num][0]
    winrate_high, winrate_low = factor_list[factor_num][1]
    profit_ratio_high, profit_ratio_low = factor_list[factor_num][2]

    # Create box plots for each row (PNL, Winrate, Profit Ratio)
    axs[0, factor_num].boxplot([pnl_high, pnl_low], labels=[f'{factor_level[factor_num][1]}', f'{factor_level[factor_num][0]}'])
    axs[0, factor_num].set_title(f"{factor_name[factor_num]} (PNL)")

    axs[1, factor_num].boxplot([winrate_high, winrate_low], labels=[f'{factor_level[factor_num][1]}', f'{factor_level[factor_num][0]}'])
    axs[1, factor_num].set_title(f"{factor_name[factor_num]} (Winrate)")

    axs[2, factor_num].boxplot([profit_ratio_high, profit_ratio_low], labels=[f'{factor_level[factor_num][1]}', f'{factor_level[factor_num][0]}'])
    axs[2, factor_num].set_title(f"{factor_name[factor_num]} (Profit Ratio)")

# Add an overall title
fig.suptitle("Box Plots for Different Factors")

# Adjust spacing between subplots
plt.tight_layout()

# Show the plot
plt.show()


# # Create a scatter plot of the first two factors
# plt.figure(figsize=(8, 6))
# plt.scatter(factor_loadings[0], factor_loadings[1], marker='o', color='b', label='Factor Loadings')
# plt.xlabel('Factor 1')
# plt.ylabel('Factor 2')
# plt.title('2^5 Factor Analysis')
# plt.grid(True)
# plt.legend()
# plt.show()
