import numpy as np
import os
import pandas as pd
import sys
import matplotlib.pyplot as plt

dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(dir))

from taker_strategy import data_parser as dp


def run_backtest(ticker, start_date, end_date, strategy_parameter):
    # Parameter set
    capital = 1000000   # USD
    position_size = 3   # %
    box_size, bollinger_length, n_std, offset, tp_box_number, sl_box_number, implement_tp_sl = strategy_parameter

    # Parse data and add simulated quotes
    df = dp.read_trade_tick_data(ticker, start_date, end_date)
    df = dp.add_simulated_bid_ask(df)

    position = 0
    trades_list = []    # [Open time, Close time, Open position, open price, close price, pnl]
    pnl_list = []

    # Start backtesting
    for row in range(len(df)):
        if row == 0 or df["Collection Time"][row].day != df["Collection Time"][row-1].day:  # Check if new day
            renko = [[round((df["Price"][row] // box_size) * box_size, 5)]*4]     # OHLC
            trend = [0]     # trend of renko box
            bb = []         # bollinger band

        if df["Tick Type"][row] == "Q":     # Submit orders per strategy
            if len(renko) >= bollinger_length + offset:
                """
                Simplest version: buy when crossing above bb and vice versa
                """
                if not implement_tp_sl:
                    if renko[-3][3] < bb[-offset-1][0] and renko[-2][3] > bb[-1-offset][0] and position <= 0: # buy condition
                        if position != 0: # close short position
                            trades_list[-1][1] = df["Collection Time"][row]
                            trades_list[-1][4] = df["Ask Price"][row]
                            trades_list[-1][5] = (trades_list[-1][4] - trades_list[-1][3]) * trades_list[-1][2]
                            pnl_list.append(trades_list[-1][5])
                            capital += pnl_list[-1]

                        position = (capital * 0.01 * position_size) // df["Ask Price"][row]
                        trades_list.append([df["Collection Time"][row], "", position, df["Ask Price"][row], np.nan, np.nan])

                    elif renko[-3][3] > bb[-offset-1][2] and renko[-2][3] < bb[-1-offset][2] and position >= 0: # sell condition
                        if position != 0: # close long position
                            trades_list[-1][1] = df["Collection Time"][row]
                            trades_list[-1][4] = df["Bid Price"][row]
                            trades_list[-1][5] = (trades_list[-1][4] - trades_list[-1][3]) * trades_list[-1][2]
                            pnl_list.append(trades_list[-1][5])
                            capital += pnl_list[-1]

                        position = -(capital * 0.01 * position_size) // df["Bid Price"][row]
                        trades_list.append([df["Collection Time"][row], "", position, df["Bid Price"][row], np.nan, np.nan])

                """
                TP/SL version
                """
                if implement_tp_sl:
                    # Check if ongoing order could be closed
                    if position > 0 and (df["Bid Price"][row] >= trades_list[-1][3] + box_size * tp_box_number or df["Bid Price"][row] <= trades_list[-1][3] - box_size * sl_box_number):
                        # TP/SL long
                        position = 0
                        trades_list[-1][1] = df["Collection Time"][row]
                        trades_list[-1][4] = df["Bid Price"][row]
                        trades_list[-1][5] = (trades_list[-1][4] - trades_list[-1][3]) * trades_list[-1][2]
                        pnl_list.append(trades_list[-1][5])
                        capital += pnl_list[-1]

                    elif position < 0 and (df["Ask Price"][row] <= trades_list[-1][3] - box_size * tp_box_number or df["Ask Price"][row] >= trades_list[-1][3] + box_size * sl_box_number):
                        # TP/SL short
                        position = 0
                        trades_list[-1][1] = df["Collection Time"][row]
                        trades_list[-1][4] = df["Ask Price"][row]
                        trades_list[-1][5] = (trades_list[-1][4] - trades_list[-1][3]) * trades_list[-1][2]
                        pnl_list.append(trades_list[-1][5])
                        capital += pnl_list[-1]


                    if renko[-3][3] < bb[-offset-1][0] and renko[-2][3] > bb[-1-offset][0] and position == 0: # buy condition
                        position = (capital * 0.01 * position_size) // df["Ask Price"][row]
                        trades_list.append([df["Collection Time"][row], "", position, df["Ask Price"][row], np.nan, np.nan])

                    elif renko[-3][3] > bb[-offset-1][2] and renko[-2][3] < bb[-1-offset][2] and position == 0: # sell condition
                        position = -(capital * 0.01 * position_size) // df["Bid Price"][row]
                        trades_list.append([df["Collection Time"][row], "", position, df["Bid Price"][row], np.nan, np.nan])

        elif df["Tick Type"][row] == "T":   # Update indicators
            price = df["Price"][row]

            if (price >= renko[-1][0] + box_size and trend[-1] != -1) or (price >= renko[-1][0] + 2*box_size and trend[-1] == -1):      # New bullish box
                # Close box and start new one
                if trend[-1] == -1:
                    renko[-1][0] = round(renko[-1][0] + box_size, 5)

                new_open = round(renko[-1][0] + box_size, 5)
                renko[-1][1] = new_open
                renko[-1][3] = new_open
                renko.append([new_open, price, new_open, price])
                trend.append(1)

                # Compute bollinger band if possible
                if len(renko) > bollinger_length + 1:
                    center = np.average([(i[1]+i[2]+i[3])/3 for i in renko[-1-bollinger_length:-1]])
                    std = np.std([(i[1]+i[2]+i[3])/3 for i in renko[-1-bollinger_length:-1]])

                    bb.append([center+std*n_std, center, center-std*n_std])

                else:
                    bb.append([np.nan, np.nan, np.nan])

            elif (price <= renko[-1][0] - box_size and trend[-1] != 1) or (price <= renko[-1][0] - 2*box_size and trend[-1] == 1):      # New bearish box
                # Close box and start new one
                if trend[-1] == -1:
                    renko[-1][0] = round(renko[-1][0] - box_size, 5)

                new_open = round(renko[-1][0] - box_size, 5)
                renko[-1][2] = new_open
                renko[-1][3] = new_open
                renko.append([new_open, new_open, price, price])
                trend.append(-1)

                # Compute bollinger band if possible
                if len(renko) > bollinger_length + 1:
                    center = np.average([(i[1]+i[2]+i[3])/3 for i in renko[-1-bollinger_length:-1]])
                    std = np.std([(i[1]+i[2]+i[3])/3 for i in renko[-1-bollinger_length:-1]])

                    bb.append((center+std*n_std, center, center-std*n_std))

                else:
                    bb.append([np.nan, np.nan, np.nan])

            else:   # No new box; update current renko box
                # update renko list
                if price > renko[-1][1]:
                    renko[-1][1] = price
                elif price < renko[-1][2]:
                    renko[-1][2] = price

                renko[-1][3] = price

        else:
            print(f"Invalid backtest data provided with tick type {df['Tick Type'][row]}")
            return
        
        # Close any open position at the end of the day
        if (row == len(df) - 1 or df["Collection Time"][row].day != df["Collection Time"][row+1].day) and position != 0:
            year_ = df["Collection Time"][row].year
            month_ = df["Collection Time"][row].month
            day_ = df["Collection Time"][row].day
            trades_list[-1][1] = pd.Timestamp(year=year_, month=month_, day=day_, hour=20, minute=00, second=00)

            if position > 0:    # close long position
                trades_list[-1][4] = df["Bid Price"][row-1]

            else:
                trades_list[-1][4] = df["Ask Price"][row-1]

            trades_list[-1][5] = (trades_list[-1][4] - trades_list[-1][3]) * trades_list[-1][2]
            pnl_list.append(trades_list[-1][5])
            capital += pnl_list[-1]


    cumulative_pnl = []
    pnl_sum = 0

    for pnl in pnl_list:
        pnl_sum += pnl
        cumulative_pnl.append(pnl_sum)

    # Calculate winrate (percentage of positive PnL trades)
    num_winning_trades = sum(pnl > 0 for pnl in pnl_list)
    total_trades = len(pnl_list)
    winrate = num_winning_trades / total_trades

    # Calculate average win and average loss (including break-even trades)
    positive_pnls = [pnl for pnl in pnl_list if pnl >= 0]
    negative_pnls = [pnl for pnl in pnl_list if pnl < 0]
    avg_win = sum(positive_pnls) / len(positive_pnls) if positive_pnls else 0
    avg_loss = sum(negative_pnls) / len(negative_pnls) if negative_pnls else 0

    # Calculate profit ratio (average win / average loss)
    profit_ratio = avg_win / abs(avg_loss) if avg_loss != 0 else float('inf')


    return trades_list, pnl_list, cumulative_pnl, winrate, profit_ratio
