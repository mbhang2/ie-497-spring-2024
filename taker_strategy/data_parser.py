import gzip
import os
import numpy as np
import pandas as pd
import random


# Converts string timestamp to pandas timestamp
def to_timestamp(input):
    time = input.split(" ")
    return pd.to_datetime(time[0] + 'T' + time[1] + 'Z', format='%Y-%m-%dT%H:%M:%S.%fZ')


def read_trade_tick_data(ticker, start_date, end_date, **kwargs):
    date = pd.to_datetime(start_date, format='%Y%m%d')

    include_premarket = kwargs.get("include_premarket", False)

    data_list = []

    while date != pd.to_datetime(end_date, format='%Y%m%d')+pd.Timedelta(days=1):
        """
        FILE PATH SUBJECT TO CHANGE!!!
        """
        file_path = f"./data/trade_tick/{ticker.upper()}/{ticker.upper()}_{date.year}{date.month if date.month >= 10 else '0'+str(date.month)}{date.day if date.day >= 10 else '0'+str(date.day)}.txt"

        # Parse file if path exists
        if (os.path.isfile(file_path)) and os.access(file_path, os.R_OK):

            with open(file_path, 'r') as file:
                for row in file:
                    row_list = row.split(',')
                    timestamp = to_timestamp(row_list[0])
                    market_open = pd.Timestamp(year=2023, month=11, day=7, hour=13, minute=30).time()
                    market_close = pd.Timestamp(year=2023, month=11, day=7, hour=20, minute=00).time()
                    
                    if include_premarket:
                        if timestamp.time() < market_close:
                            data_list.append([timestamp,float(row_list[5]), int(row_list[6][0:-1]), row_list[3]])
                    else:
                        if timestamp.time() > market_open and timestamp.time() < market_close:
                            data_list.append([timestamp,float(row_list[5]), int(row_list[6][0:-1]), row_list[3]])
                        

        date += pd.Timedelta(days=1)

    data_df = pd.DataFrame(data_list, columns=['Collection Time', 'Price', 'Size', 'Tick Type'])

    if len(data_df) == 0:
        print("ERROR: File not found")

    data_df.sort_values(by=["Collection Time"], inplace=True)
    data_df.reset_index(drop=True, inplace=True)

    return data_df


def add_simulated_bid_ask(dataframe, **kwarg):
    # Add new columns
    dataframe[["Bid Price", "Bid Size", "Ask Price", "Ask Size"]] = 0
    market_open = pd.Timestamp(year=2023, month=11, day=7, hour=13, minute=30).time()

    initial_num_rows = len(dataframe)
    for index in range(initial_num_rows):
        if index == 0 or dataframe["Collection Time"][index].day != dataframe["Collection Time"][index-1].day or dataframe["Collection Time"][index-1].time() < market_open:
            continue

        collection_time = dataframe["Collection Time"][index] - pd.Timedelta(microseconds=0.001)
        bid_price = dataframe["Price"][index] - np.random.choice([0.0, 0.01, 0.02])
        ask_price = dataframe["Price"][index] + np.random.choice([0.0, 0.01, 0.02])

        while bid_price == ask_price:
            bid_price = dataframe["Price"][index] - np.random.choice([0.0, 0.01, 0.02])
            ask_price = dataframe["Price"][index] + np.random.choice([0.0, 0.01, 0.02])

        bid_size = int(dataframe["Size"][index] * (np.random.gamma(2, 0.3)+1))
        ask_size = int(dataframe["Size"][index] * (np.random.gamma(2, 0.3)+1))

        dataframe.loc[len(dataframe)] = [collection_time, 0, 0, "Q", bid_price, bid_size, ask_price, ask_size]

    dataframe.sort_values(by=["Collection Time"], inplace=True)
    dataframe.reset_index(drop=True, inplace=True)

    return dataframe

