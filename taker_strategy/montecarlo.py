import backtest
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def perfrom_montecarlo_simulation(ticker, start_date, end_date, strategy_parameter, num):
    pnl_summary = []
    cumulative_pnl_summary = []
    winrate_summary = []
    profit_ratio_summary = []

    for i in range(num):
        trades_list, pnl_list, cumulative_pnl, winrate, profit_ratio = backtest.run_backtest(ticker, start_date, end_date, strategy_parameter)

        pnl_summary.append(pnl_list)
        cumulative_pnl_summary.append(cumulative_pnl)
        winrate_summary.append(winrate)
        profit_ratio_summary.append(profit_ratio)

    plt.figure(figsize=(10, 6))
    for i, sim in enumerate(cumulative_pnl_summary):
        plt.plot(sim, label=f'Simulation {i+1}')

    plt.xlabel('Trade Number')
    plt.ylabel('Cumulative PnL')
    plt.title('Monte Carlo Simulations')
    plt.grid(True)
    plt.legend()
    plt.show()

    return pnl_summary, cumulative_pnl_summary, winrate_summary, profit_ratio_summary


def perfrom_montecarlo_simulation_with_strategy_options(ticker, start_date, end_date, options, num, output_path):
    result_df = pd.DataFrame(columns = ["Box Size", "BB Length", "SD", "BB Offset", "TP", "SL", "Avg Cumulative Pnl", "Avg Winrate", "Avg Profit Ratio"])

    for option in options:
        pnl_summary, cumulative_pnl_summary, winrate_summary, profit_ratio_summary = perfrom_montecarlo_simulation(ticker, start_date, end_date, option, num)

        avg_cum_pnl = round(np.mean([i[-1] for i in cumulative_pnl_summary]),2)

        result_df.loc[len(result_df)] = [option[0], option[1], option[2], option[3], option[4] if option[6] else -1, option[5] if option[6] else -1, avg_cum_pnl, np.mean(winrate_summary), np.mean(profit_ratio_summary)]

    result_df.to_csv(output_path)