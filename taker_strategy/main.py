import montecarlo


"""
Configuration
"""
# Backtest
ticker = "SPY"
start_date = "20191001"
end_date = "20191101"
num_simulation = 10
path = f"./Python/taker-strategy/results/{ticker}_{start_date}_{end_date}_result.csv"

# Strategy
box_size = [0.05, 0.1]
bollinger_band_length = [5, 10]
standard_deviation = [1, 2]
bollinger_band_offset = [0, 2]
take_profit_num_boxes = 5
stop_loss_num_boxes = 2
use_tp_sl = [True, False]

strategy_options = []

for i in box_size:
    for j in bollinger_band_length:
        for k in standard_deviation:
            for l in bollinger_band_offset:
                for m in use_tp_sl:
                    strategy_options.append([i,j,k,l,take_profit_num_boxes, stop_loss_num_boxes, m])

"""
Run Monte Carlo Simulation
"""
# pnl_summary, cumulative_pnl_summary, winrate_summary, profit_ratio_summary = montecarlo.perfrom_montecarlo_simulation(ticker, start_date, end_date, 
#                     [0.1, 5, 1, 0, 5, 2, True], num_simulation)

# montecarlo.perfrom_montecarlo_simulation_with_strategy_options(ticker, start_date, end_date, strategy_options, num_simulation, path)


"""
Plot
"""
# plt.figure(figsize=(10, 6))
# for i, sim in enumerate(cumulative_pnl_summary):
#     plt.plot(sim, label=f'Simulation {i+1}')

# plt.xlabel('Trade number')
# plt.ylabel('Cumulative PnL')
# plt.title('Monte Carlo Simulations')
# plt.grid(True)
# plt.legend()
# plt.show()
